const coursesRouter = require('express').Router()
const Course = require('../models/course')
const User = require('../models/user')

coursesRouter.get('/', async (request, response) => {
    const courses = await Course.find({}).populate('users', {
        username: 1,
        name: 1,
        email: 1,
        contact: 1, 
    })
    response.json(courses.map((course) => course.toJSON()));
})

coursesRouter.get('/:id', async (request, response) => {
    const course = await Course.findById(request.params.id)
    if(course) {
        response.json(course)
    }
    else {
        response.status(404).end()
    }
})

coursesRouter.post('/', async (request, response) => {
    const { title, description, startDate, endDate } = request.body
    
    const course = new Course({
        title,
        description,
        startDate,
        endDate,
    })

    const savedCourse = await course.save()
    response.status(201).json(savedCourse.toJSON())
})

module.exports = coursesRouter