const bcrypt = require('bcrypt')
const usersRouter = require("express").Router();
const User = require("../models/user");
const Course = require("../models/course")

usersRouter.get('/', async (request, response) => {
    const users = await User.find({}).populate('courses', {
        title: 1,
        description: 1,
        startDate: 1,
        endDate: 1,
    })
    response.json(users.map((user) => user.toJSON()));
})

usersRouter.get('/:id', async (request, response) => {
    const user = await User.findById(request.params.id)
    if(user) {
        response.json(user)
    }
    else {
        response.status(404).end()
    }
})

usersRouter.post('/', async (request, response) => {
    const { username, name, password, email, contact } = request.body

    if(!password) {
        return response.status(400).json({
            error: 'passwod is required'
        })
    }

    if(!username) {
        return response.status(400).json({
            error: 'username is required'
        })
    }

    if(!email) {
        return response.status(400).json({
            error: 'email is required'
        })
    }

    if(!contact) {
        return response.status(400).json({
            error: 'contact is required'
        })
    }

    if(password.length < 8) {
        return response.status(400).json({
          error: 'password must be at least 8 characters'
        })
    }
    
    if(username.length < 5) {
        return response.status(400).json({
          error: 'username must be at least 5 characters',
        })
    }

    const existingUsername = await User.find({ username: username })

    if(existingUsername.length > 0) {
        return response.status(400).json({
            error: 'username must be unique'
        })
    }

    const existingContact = await User.find({ contact })

    if(existingContact.length > 0) {
        return response.status(400).json({
            error: 'contact must be unique'
        })
    }

    const existingEmail = await User.find({ email })
    
    if(existingEmail.length > 0) {
        return response.status(400).json({
            error: 'email must be unique'
        })
    }

    const saltRound = 10
    const passwordHash = await bcrypt.hash(password, saltRound)

    const user = new User({
        username,
        name,
        passwordHash,
        email,
        contact,
    })

    const savedUser = await user.save()
    response.status(201).json(savedUser)
})

usersRouter.put('/:id', async (request, response) => {
    const user = await User.findById(request.params.id)
    if(user) {
        const { courseId } = request.body
        const course = await Course.findById(courseId)

        const currentDate = new Date()

        if(currentDate > course.startDate) {
            return response.status(400).json({
                error: 'course has already started'
            })
        }

        user.courses.push(courseId)
        const savedUser = await User.findByIdAndUpdate(request.params.id, user, { new: true })
        
        course.users.push(savedUser.id)
        await course.save()

        response.json(savedUser)
    }
    else {
        response.status(404).end()
    }
})

module.exports = usersRouter