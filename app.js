const express = require('express')
const app = express()
const config = require('./utils/config');
const mongoose = require('mongoose');
const usersRouter = require('./controllers/users')
const coursesRouter = require('./controllers/courses')

const url = config.MONGODB_URI;
console.log('Connecting to MongoDb')

mongoose
    .connect(url)
    .then((result) => {
        console.log('Connected to MongoDb')
    })
    .catch((error) => {
        console.log('Error connecting to MongoDb:', error.message)
    })

app.use(express.json())

app.use('/api/users', usersRouter)
app.use('/api/courses', coursesRouter)

module.exports = app